import { Component } from '@angular/core';
import { ChatService } from './app.chat.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'rxjs-chat';
  message: string;
  messages: string[] = [];


  constructor(private chatService: ChatService){

  }

  ngOnInit() {

    this.chatService
      .getMessages()
      .subscribe((message: string) => {
        this.messages.push(message);

        console.log(message);
      });
  }
}
