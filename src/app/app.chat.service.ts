import * as io from 'socket.io-client';
import { Observable } from "rxjs";

export class ChatService {
  private url = 'http://192.168.137.98:8076';//http://localhost:3000';
  private socket;

  constructor() {
    this.socket = io(this.url);
  }

  public getMessages = () => {
    return Observable.create((observer) => {
      this.socket.on('new-message', (message) => {
        observer.next(message);
      });
    });
  }


}
